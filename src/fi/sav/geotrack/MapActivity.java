package fi.sav.geotrack;

import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import fi.sav.geotrack.data.DatabaseManager;
import fi.sav.geotrack.data.GTLocation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

public class MapActivity extends Activity {
	
	private GoogleMap			geoMap;
	private Long				selectedLocationId;
	private List<GTLocation>	locations;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
			
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		Intent intent = getIntent();
		this.selectedLocationId = intent.getLongExtra("fi.sav.geotrack.LocationId", -1);	
		
		initMap();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	/**
	 * Initializes map
	 */
	public void initMap() {
		
		this.geoMap = ( (MapFragment) getFragmentManager().findFragmentById(R.id.map) ).getMap();			
				
		// Get locations from the database and draw markers into map
		DatabaseManager dbm = new DatabaseManager(this);
		this.locations = dbm.getLocations();
		
		if(locations.size() > 0) {
			
			for(GTLocation location : this.locations) {
				
				if(this.selectedLocationId == -1) {
					
					addMarkerByLocation(location);
				}
				else if(this.selectedLocationId == location.getId()) {
					
					addMarkerByLocation(location);
				}
			}
			
			// Set the camera position to the last location in the location list
			setCameraPositionByLocation(this.locations.get(this.locations.size() - 1));
		}
		else {
			
			Toast.makeText(this, "There are no locations!", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
		
	/**
	 * @param location
	 * 
	 * Set maps camera position by the given GTLocation-object.
	 */
	public void setCameraPositionByLocation(GTLocation location) {
		
		LatLng coords = new LatLng( Double.parseDouble(location.getLatitude()), Double.parseDouble(location.getLongitude()) );
		CameraPosition camPos = new CameraPosition(coords, 14, 0, 0);
		
		geoMap.moveCamera(CameraUpdateFactory.newCameraPosition(camPos));	
	}
	
	/**
	 * @param location
	 * 
	 * Adds marker to map by the given GTLocation-object.
	 */
	public void addMarkerByLocation(GTLocation location) {
		
		LatLng coords = new LatLng( Double.parseDouble(location.getLatitude()), Double.parseDouble(location.getLongitude()) );
				
		MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.position(coords);
		markerOptions.title(location.getDate());
		markerOptions.snippet("Lat: " + coords.latitude + " Lng: " + coords.longitude);
		
		geoMap.addMarker(markerOptions);
	}
	
}

package fi.sav.geotrack;

import fi.sav.geotrack.data.DatabaseManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;


public class MainActivity extends Activity {
	
	private DatabaseManager		databaseManager;
	private LocationManager		locationManager;
	private LocationListener	locationListener;
	private float				minLocationUpdateDistance 	= 20;	// Meters
	private long				minLocationUpdateTime 		= 2000;	// Milliseconds
	private ToggleButton		trackBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.databaseManager = new DatabaseManager(this);
		
		initUI();
		initLocationManagerAndListener();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// Do nothing when Back-button is pressed
	}	
	
	/**
	 *	Initializes needed UI references
	 */
	private void initUI() {
		
		this.trackBtn = (ToggleButton) findViewById(R.id.trackBtn);
	}
	
	/**
	 * @param btnView
	 */
	public void trackBtnHandler(View btnView) {
					
		boolean on = ((ToggleButton) btnView).isChecked();
		
		if(on) {
			enableLocationTracking();
		}
		else {
			disableLocationTracking();
		}		
	}
		
	/**
	 * @param btnView
	 */
	public void mapBtnHandler(View btnView) {	
		
		Intent intent = new Intent(this, MapActivity.class);
		startActivity(intent);
	}
	
	/**
	 * @param btnView
	 */
	public void listBtnHandler(View btnView) {
		
		Intent intent = new Intent(this, ListActivity.class);
		startActivity(intent);
	}
	
	
	/**
	 * Initializes LocationManager and LocationListener
	 * 
	 */
	public void initLocationManagerAndListener() {
		
		this.locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		
		this.locationListener = new LocationListener() {
				
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// Do nothing
			}
			
			@Override
			public void onProviderEnabled(String provider) {
				
				Toast.makeText(MainActivity.this, "GPS provider enabled", Toast.LENGTH_SHORT).show();		
				
			}
			
			@Override
			public void onProviderDisabled(String provider) {
				
				Toast.makeText(MainActivity.this, "GPS provider disabled", Toast.LENGTH_SHORT).show();
			}
			
			@Override
			public void onLocationChanged(Location location) {

				storeLocation(location);
			}
			
		};	
	}
	
	/**
	 *	Checks if GPS-provider is enabled. If it is, requests location updates.
	 *	If it's not, shows toast for requesting user to enable it.  
	 */
	public void enableLocationTracking() {
		
		if(this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			
			Toast.makeText(this, "Requesting GPS location...", Toast.LENGTH_SHORT).show();
			this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, this.minLocationUpdateTime, this.minLocationUpdateDistance, this.locationListener);
			
		} else {
			
			this.trackBtn.setChecked(false);
			Toast.makeText(this, "GPS is disabled. Please enable it to track locations!", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 *	Disables location updates 
	 */
	public void disableLocationTracking() {
		
		this.locationManager.removeUpdates(this.locationListener);
	}
	
	/**
	 * @param location
	 */
	public void storeLocation(Location location) {
		
		this.databaseManager.insertLocation(location);
	}
}

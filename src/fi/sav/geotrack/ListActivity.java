package fi.sav.geotrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.sav.geotrack.data.DatabaseManager;
import fi.sav.geotrack.data.GTLocation;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class ListActivity extends Activity {
	
	private DatabaseManager		databaseManager;
	private ListView			locationsListView;
	private SimpleAdapter 		listAdapter;
	private List<GTLocation> 	locations;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		
		this.databaseManager = new DatabaseManager(this);
		
		initUI();
		updateListView();		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		return true;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		
		super.onCreateContextMenu(menu, v, menuInfo);
		
		menu.add(0, Menu.FIRST, 0, "Show location");
		menu.add(0, Menu.FIRST + 1, 0, "Delete location");
		menu.add(0, Menu.FIRST + 2, 0, "Cancel");	
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();		
		long selectedLocationId = this.locations.get(info.position).getId();
		
		switch (item.getItemId()) {
		
			case 1:
						
				Intent intent = new Intent(this, MapActivity.class);
				intent.putExtra("fi.sav.geotrack.LocationId", selectedLocationId);
				startActivity(intent);
				
				break;
			
			case 2:
				
				this.databaseManager.deleteLocation(selectedLocationId);
				this.locationsListView.clearChoices();
				updateListView();
				
				break;
				
			case 3:
				// Cancel
				break;
				
			default:
				break;
			
		}
		
		return super.onContextItemSelected(item);
	}
	
	private void initUI() {
		
		this.locationsListView = (ListView) findViewById(R.id.locationsListView);
	}
	
	private void updateListView() {
			
		this.locations = this.databaseManager.getLocations();
		
		if(this.locations.size() > 0) {
			
			ArrayList<Map<String, String>> adapterList = new ArrayList<Map<String,String>>();
			
			for(GTLocation location : this.locations) {
				
				HashMap<String, String> locMap = new HashMap<String, String>();
				locMap.put("latLng", "Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());
				locMap.put("date", location.getDate());
				adapterList.add(locMap);
			}
			
			String[] from	= new String[] {"latLng", "date"};
			int[] to 		= {R.id.listRowLatLng, R.id.listRowDate};
			
			this.listAdapter = new SimpleAdapter(this, adapterList, R.layout.list_row, from, to);
			this.locationsListView.setAdapter(this.listAdapter);
			
			registerForContextMenu(this.locationsListView);
		}
		else {
			
			Toast.makeText(this, "There are no locations!", Toast.LENGTH_LONG).show();
		}
	}	
	
	
}

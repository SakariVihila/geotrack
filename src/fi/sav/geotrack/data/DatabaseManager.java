package fi.sav.geotrack.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.text.format.Time;

public class DatabaseManager extends SQLiteOpenHelper {
	
	private static final String	DATABASE_NAME 		= "geotrack_database";
	private static final int	DATABASE_VERSION 	= 1;
	
	private static final String	TABLE_LOCATION		= "geotrack_location";
	private static final String	COL_ID				= "ID";
	public static final String	COL_LATITUDE		= "LATITUDE";
	private static final String	COL_LONGITUDE		= "LONGITUDE";
	private static final String	COL_INSERTED		= "INSERTED";
	
	private static final String DATABASE_CREATE 	= "CREATE TABLE " + TABLE_LOCATION + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LATITUDE + " TEXT NOT NULL, " + COL_LONGITUDE + " TEXT NOT NULL, " + COL_INSERTED + " TEXT NOT NULL);";        
    private static final String DATABASE_DROP 		= "DROP TABLE IF EXISTS " + TABLE_LOCATION;
	
	
	public DatabaseManager(Context context) {
		
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL(DATABASE_CREATE); 
	}

 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		db.execSQL(DATABASE_DROP);
		onCreate(db);
	}
	
	/**
	 * @param Location location
	 * 
	 * @return long locationId
	 * 
	 * Inserts location data into local database by the given Location-object
	 * and returns the new location id.
	 */
	public long insertLocation(Location location) {
		
		ContentValues values = new ContentValues();
				
		values.put( COL_LATITUDE, String.valueOf(location.getLatitude()) );
		values.put( COL_LONGITUDE, String.valueOf(location.getLongitude()) );
		values.put( COL_INSERTED, getDateString() );
		
		long locationId = this.getWritableDatabase().insert(TABLE_LOCATION, null, values);
		
		this.close();	
		return locationId;
	}
	
	/**
	 * @param locationId
	 */
	public void deleteLocation(long locationId) {
		
		this.getWritableDatabase().delete(TABLE_LOCATION, COL_ID + " = " + locationId, null);
		this.close();
	}
	
	/**
	 * @return
	 * 
	 * Returns locations from local database as ArrayList<GTLocation>.
	 */
	public List<GTLocation> getLocations() {
		
		List<GTLocation> locations = new ArrayList<GTLocation>();
		
		String[] tableCols = new String[] {COL_ID, COL_LATITUDE, COL_LONGITUDE, COL_INSERTED};
		Cursor cursor = this.getWritableDatabase().query(TABLE_LOCATION, tableCols, null, null, null, null, null);
		
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast()) {
			
			locations.add(createLocationFromCursor(cursor));
			cursor.moveToNext();
		}
		
		cursor.close();
		this.close();
		
		return locations;
	}
	
	private GTLocation createLocationFromCursor(Cursor cursor) {
		
		GTLocation location = new GTLocation();
		
		location.setId(cursor.getLong(0));
		location.setLatitude(cursor.getString(1));
		location.setLongitude(cursor.getString(2));
		location.setDate(cursor.getString(3));		
		
		return location;
	}
	
	/**
	 * @return String date
	 * 
	 * Returns current date as String (mm/dd/yyyy).
	 */
	private String getDateString() {
		
		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();
		        
		StringBuilder sb = new StringBuilder();
		        
		sb.append(today.monthDay);
		sb.append("/");
		sb.append(today.month + 1);
		sb.append("/");
		sb.append(today.year);
		
		return sb.toString();
	}
	
	
}
